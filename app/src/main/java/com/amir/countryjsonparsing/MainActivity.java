package com.amir.countryjsonparsing;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class MainActivity extends AppCompatActivity {

    String TAG = MainActivity.class.getSimpleName();
    ProgressDialog pDialog;
    ListView lv;
    ArrayList<Data> dataArrayList;
    /*    ArrayList<String> countryList;
        ArrayList<String> countryCodeList;
        ArrayList<String> capitalList;
        ArrayList<Double> latList;
        ArrayList<Double> lonList;*/
    RelativeLayout detailLayout;
    RelativeLayout detailMask;
    String countryName;
    String countryCode;
    ImageView imageView;
    TextView countryText;
    private static String url;
    String nameValue;
    TextView capitalText;
    TextView capitalWeather;
    ImageView weatherIcon;
    Double latValue;
    Double lonValue;
    ImageView runMapView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // URL to get country info from JSON
        url = "http://www.geognos.com/api/en/countries/info/all.json";

        lv = (ListView) findViewById(R.id.list);
        detailLayout = (RelativeLayout) findViewById(R.id.detailLayout);
        detailMask = (RelativeLayout) findViewById(R.id.detailMask);
        imageView = (ImageView) findViewById(R.id.imageView);
        countryText = (TextView) findViewById(R.id.countryText);
        capitalText = (TextView) findViewById(R.id.capitalText);
        capitalWeather = (TextView) findViewById(R.id.capitalWeather);
        weatherIcon = (ImageView) findViewById(R.id.weatherIcon);
        runMapView = (ImageView) findViewById(R.id.runMapView);
        new GetCoutry().execute();
        dataArrayList = new ArrayList<>();
/*        countryList = new ArrayList<>();
        countryCodeList = new ArrayList<>();
        capitalList = new ArrayList<>();
        // lv.setEnabled(false);

        //Clear lists
        capitalList.clear();
        countryList.clear();
        countryCodeList.clear();
        latList = new ArrayList<>();
        lonList = new ArrayList<>();*/


    }

    public void closeDetailLayout(View view) {
        detailLayout.setVisibility(View.GONE);
        detailMask.setVisibility(View.GONE);
        lv.setEnabled(true);
    }

    public void runMapView(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("LAT", latValue);
        intent.putExtra("LON", lonValue);
        intent.putExtra("COUNTRY_NAME", countryText.getText().toString());
        startActivity(intent);
        detailLayout.setVisibility(View.GONE);
        detailMask.setVisibility(View.GONE);
        lv.setEnabled(true);

    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetCoutry extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage(getString(R.string.please_wait));
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url);
            //   Log.e(TAG, "Response from url: " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jObject = new JSONObject(jsonStr).getJSONObject("Results");
                    Iterator<String> keys = jObject.keys();
                    while (keys.hasNext()) {
                        //Niz objekata
                        String key = keys.next();
                        //   Log.v("category key ", key);
                        JSONObject innerJObject = jObject.getJSONObject(key);
                        //Objekat kao string
                        countryCode = key;
                        //String unutar objekta
                        countryName = innerJObject.getString("Name");
                        //Log.v("Country name ", countryName);
                        //countryCodeList.add(countryCode);


                        try {
                            //Objekat unutar objekta
                            JSONObject capitalObject = innerJObject.getJSONObject("Capital");

                            double lat = (double) capitalObject.getJSONArray("GeoPt").get(0);
                            double lon = (double) capitalObject.getJSONArray("GeoPt").get(1);

                            //String unutar >>Capital<< objekta
                            nameValue = capitalObject.getString("Name");
                            // capitalList.add(nameValue);
                            dataArrayList.add(new Data(countryName, countryCode, nameValue, lat, lon));

                            Log.v("Koordinate", lat + "," + lon);
                            //Ako nema podataka
                        } catch (Exception e) {
                            //Log.i(TAG, "Cant find Capital object");
                            dataArrayList.add(new Data(countryName, countryCode, "No data", 0.0, 0.0));

                        }
                    }

                } catch (final JSONException e) {
                    Log.e(TAG, getString(R.string.parisng_error) + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    R.string.parisng_error + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            } else {
                Log.e(TAG, getString(R.string.couldn_get_json));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                R.string.couldn_get_json,
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            //New capitalName list for adapter
            List<String> strings = new ArrayList<>();
            for (Data dataList : dataArrayList) {
                strings.add(dataList.getCountryList());
            }

            // Log.i(TAG, countryList.toString()); //Lista država u stringu
            lv.setAdapter(new ArrayAdapter<String>(MainActivity.this,
                    android.R.layout.simple_list_item_1, strings));

            //Na klik jednog elementa liste
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    detailLayout.setVisibility(View.VISIBLE);
                    detailMask.setVisibility(View.VISIBLE);
                    lv.setEnabled(false);
                    Picasso.with(MainActivity.this)
                            .load("http://www.geognos.com/api/en/countries/flag/" + dataArrayList.get(position).getCountryCodeList() + ".png")
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.error_image)
                            .into(imageView);
                    //Toast.makeText(MainActivity.this, "You are clicked " + countryList.get(position) + countryCodeList.get(position) + capitalList.get(position), Toast.LENGTH_LONG).show();
                    countryText.setText(dataArrayList.get(position).getCountryList());
                    capitalText.setText(dataArrayList.get(position).getCapitalList());

                    latValue = dataArrayList.get(position).getLatList();
                    lonValue = dataArrayList.get(position).getLongList();

                    //Ako nema podataka o glavnom gradu ne prikazuj prognozu i iskljuci lokaciju grada
                    if (capitalText.getText().equals("No data")) {
                        runMapView.setEnabled(false);
                        weatherIcon.setVisibility(View.GONE);
                        capitalWeather.setVisibility(View.GONE);
                    } else {
                        runMapView.setEnabled(true);
                        weatherIcon.setVisibility(View.VISIBLE);
                        capitalWeather.setVisibility(View.VISIBLE);

                    }
                    //  Toast.makeText(MainActivity.this, "You are clicked " + latValue + lonValue, Toast.LENGTH_LONG).show();
                    //Vremenska prognoza za glavni grad
                    DownloadClass task = new DownloadClass();
                    try {
                        task.execute("http://api.openweathermap.org/data/2.5/weather?q=" + capitalText.getText() + "&APPID=bd5e378503939ddaee76f12ad7a97608").get();
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }


    // Dohvati vremensku prognozu
    private class DownloadClass extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... urls) {
            String result = "";
            URL url;
            HttpURLConnection httpURLConnetctions = null;
            try {
                url = new URL(urls[0]);
                httpURLConnetctions = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnetctions.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                int data = inputStream.read();

                while (data != -1) {
                    char charater = (char) data;
                    result += charater;
                    data = inputStreamReader.read();
                }

                return result;

            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                JSONObject object = new JSONObject(s);
                String weather = object.getString("weather");
                JSONArray jsonArray = new JSONArray(weather);
                // Log.i("Info", weather);
                for (int i = 0; i < 1; i++) {
                    JSONObject jsonPart = jsonArray.getJSONObject(i);


                    capitalWeather.setText(jsonPart.getString("description"));
                    // curentWeather.setText(jsonPart.getString("main"));
                    String icons = jsonPart.getString("icon");
                    Picasso.with(MainActivity.this)
                            .load("http://openweathermap.org/img/w/" + icons + ".png")
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.error_image)
                            .into(weatherIcon);
                }

            } catch (JSONException e) {
                Toast.makeText(MainActivity.this, "Weather not found", Toast.LENGTH_LONG).show();
            }
        }
    }

}

package com.amir.countryjsonparsing;

/**
 * Created by KOrisn on 30.10.2017..
 */

public class Data {
    String countryList;
    String countryCodeList;
    String capitalList;
    Double latList;
    Double longList;

    public Data(String countryList, String countryCodeList, String capitalList, Double latList, Double longList) {
        this.countryList = countryList;
        this.countryCodeList = countryCodeList;
        this.capitalList = capitalList;
        this.latList = latList;
        this.longList = longList;
    }

    public String getCountryList() {
        return countryList;
    }

    public void setCountryList(String countryList) {
        this.countryList = countryList;
    }

    public String getCountryCodeList() {
        return countryCodeList;
    }

    public void setCountryCodeList(String countryCodeList) {
        this.countryCodeList = countryCodeList;
    }

    public String getCapitalList() {
        return capitalList;
    }

    public void setCapitalList(String capitalList) {
        this.capitalList = capitalList;
    }

    public Double getLatList() {
        return latList;
    }

    public void setLatList(Double latList) {
        this.latList = latList;
    }

    public Double getLongList() {
        return longList;
    }

    public void setLongList(Double longList) {
        this.longList = longList;
    }
}

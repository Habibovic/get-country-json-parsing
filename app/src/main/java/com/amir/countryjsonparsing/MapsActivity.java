package com.amir.countryjsonparsing;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    double lat, lon;
    String countryName = "";
    TextView mapTextCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapTextCountry = (TextView) findViewById(R.id.mapTextCountry);
        Intent intent = getIntent();
        lat = intent.getDoubleExtra("LAT", lat);
        lon = intent.getDoubleExtra("LON", lon);
        countryName = intent.getStringExtra("COUNTRY_NAME");
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Sets the map type to be "hybrid"
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        //mMap.getUiSettings().setZoomControlsEnabled(true);
        LatLng position = new LatLng(lat, lon);
        mMap.addMarker(new MarkerOptions()
                .position(position)
                .title(countryName)
                .snippet("Population: 4,137,400"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 7));
        mapTextCountry.setText(countryName);

    }

    public void finishMap(View view) {
        finish();
    }
}
